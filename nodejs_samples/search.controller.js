'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('../errors.server.controller'),
    Bar = mongoose.model('Bar'),
    _ = require('lodash'),
    Deals = require('../../../app/models/deals.server.model.js'),
    async = require('async');

exports.getBars = function (req, res) {

    var cityid = req.params.city;
    var radius = req.body.radius;
    var center = req.body.center;
    var dayKey = req.body.day;
    var timing = req.body.timing;

    var days = ['su', 'mo', 'tu', 'we', 'th', 'fr', 'sa'];

    var d = new Date();
    if (days.indexOf(dayKey) === -1) {
        dayKey = days[d.getDay()];
    }

    var barQuery = {
        queryMust: [],
        filter: []
    };

    barQuery.queryMust.push({
        term: {city: cityid}
    });

    if (req.body.barFilters.types.length > 0)
        barQuery.queryMust.push({
            "terms": {
                "barTypes": req.body.barFilters.types,
                "minimum_should_match": 1
            }
        });

    if (req.body.barFilters.areas.length > 0)
        barQuery.queryMust.push({
            "terms": {
                "areas": req.body.barFilters.areas,
                "minimum_should_match": 1
            }
        });
    else {
        barQuery.filter = {
            geo_distance: {
                distance: radius / 1000 + 'km',
                geo_coordinates: [
                    center.longitude,
                    center.latitude
                ]
            }
        };
    }

    Bar.search(
        {
            filtered: {
                query: {
                    bool: {
                        must: barQuery.queryMust
                    }
                },

                filter: barQuery.filter

            }
        },

        {
            'from': 0, 'size': 1000,
            'sort': [
                {
                    '_geo_distance': {
                        geo_coordinates: [
                            center.longitude,
                            center.latitude
                        ],
                        'order': 'asc',
                        'unit': 'km',
                        'distance_type': 'plane'
                    }
                }
            ]
        }, function (err, results) {

            //res.send(results);

            var bars = {};
            var ids = [];

            if (err) console.log(err);

            results.hits.hits.forEach(function (hit, i) {

                bars[hit._id] = {
                    id: i,
                    mid: hit._id,
                    latitude: hit._source.geo_coordinates[1],
                    longitude: hit._source.geo_coordinates[0],
                    name: hit._source.name,
                    areas: hit._source.areas,
                    barTypes: hit._source.barTypes,
                    distance: hit.sort[0],
                    icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png'
                };

                ids.push(hit._id);

            });

            var range = {
                from: 0,
                to: 0
            };

            if (timing.from !== '') {
                var fromInt = timing.from.replace(':', '')
                range.from = parseInt(fromInt, 10);
            }
            else
                range.from = d.getHours() * 100;

            if (timing.to !== '') {
                var toInt = timing.to.replace(':', '')
                range.to = parseInt(toInt, 10);
            }
            else
                range.to = range.from + 100;

            if (range.to < range.from)
                range.to += 2400;

            var fromRange = {};
            fromRange[dayKey + '.from'] = {
                'lt': range.from
            };

            var toRange = {};
            toRange[dayKey + '.to'] = {
                'gt': range.to
            };

            function getDealQuery(queryTerms, day, fromRange, toRange) {

                queryTerms.push({
                    terms: {
                        bar: ids
                    }
                });


                return {
                    filtered: {
                        //query: queryTerms,
                        query: {
                            bool: {
                                must: queryTerms
                            }
                        },

                        'filter': {
                            'nested': {
                                'query': {
                                    'bool': {
                                        'must': [
                                            {
                                                'range': fromRange
                                            },
                                            {
                                                'range': toRange
                                            }
                                        ]
                                    }
                                },
                                'path': dayKey
                            }
                        }
                    }


                };
            }

            var params = {
                'from': 0, 'size': 10,

                'sort': [

                    {'type': 'asc'},
                    {'priceValue': 'asc'}
                ]

            };

            var deals = {
                beer: [],
                wine: [],
                liquor: []
            };

            function addTerms(terms, fieldName, values) {

                var termsObj = {
                    terms: {
                        'minimum_should_match': 1
                    }
                };

                if (values.length > 0) {
                    termsObj['terms'][fieldName] = values;

                    terms.push(termsObj);
                }

            }

            var dealQueries = [];

            if (req.body.search.beer){
                dealQueries.push(

                    function (callback) {

                        var terms = [];
                        addTerms(terms, 'drinkCategories', req.body.beerFilters.categories);
                        addTerms(terms, 'drinkBrands', req.body.beerFilters.brands);
                        addTerms(terms, 'drinkContainers', req.body.beerFilters.containers);

                        Deals.BeerDeal.search(
                            getDealQuery(terms, dayKey, fromRange, toRange),
                            params,

                            function (e, beerDeals) {

                                if (!e) {
                                    deals.beer = beerDeals;
                                    callback(null);
                                }
                            }
                        );


                    }

                );
            }

            if (req.body.search.wine){
                dealQueries.push(

                    function (callback) {

                        var terms = [];
                        addTerms(terms, 'drinkCategories', req.body.wineFilters.categories);
                        addTerms(terms, 'drinkBrands', req.body.wineFilters.brands);
                        addTerms(terms, 'drinkContainers', req.body.wineFilters.containers);

                        Deals.WineDeal.search(
                            getDealQuery(terms, dayKey, fromRange, toRange),
                            params,

                            function (e, wDeals) {

                                if (!e) {
                                    deals.wine = wDeals;
                                    callback(null);
                                }

                            }
                        );


                    }

                );
            }

            if (req.body.search.liquor){
                dealQueries.push(

                    function (callback) {

                        var terms = [];
                        addTerms(terms, 'drinkCategories', req.body.liquorFilters.categories);
                        addTerms(terms, 'drinkBrands', req.body.liquorFilters.brands);
                        addTerms(terms, 'liquorCategories', req.body.liquorFilters.subType);

                        Deals.LiquorDeal.search(
                            getDealQuery(terms, dayKey, fromRange, toRange),
                            params,

                            function (e, lDeals) {

                                if (!e) {
                                    deals.liquor = lDeals;
                                    callback(null);
                                }

                            }
                        );


                    }

                );
            }

            async.parallel(
                dealQueries,

                function (err, results) {

                    res.send({
                        bars: bars,
                        deals: deals

                    });

                }
            );


            //Bar.find({city: cityid}, '-__v')
            //
            //    .populate([
            //        {path: 'tags', select: '_id, name'},
            //        {path: 'areas', select: '_id, name'},
            //        {path: 'cuisines', select: '_id, name'},
            //        {path: 'barTypes', select: '_id, name'}
            //    ])
            //
            //    .exec(function (err, deals) {
            //
            //        res.send(deals);
            //
            //    });
        });


};
