'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./errors.server.controller'),
    Bar = mongoose.model('Bar'),
    _ = require('lodash'),
    ObjectId = require('mongoose').Types.ObjectId,
    GeneralModels = require('../../app/models/generalModels.server.model.js'),
    Deals = require('../../app/models/deals.server.model.js'),
    async = require('async');


exports.addDealFields = function (req, res) {

    var list = req.body.list;
    var type = req.body.typeid;
    var fieldName = req.params.field;

    var rows = list.match(/[^\r\n]+/g);

    var items = [];

    var item = '';
    var itemObj;
    for (var i = 0; i < rows.length; i++) {
        item = rows[i].trim();

        if (item !== '') {
            itemObj = {
                name: item
            };

            if (type !== undefined)
                itemObj.drinkType = new ObjectId(type);

            items.push(itemObj);
        }

    }

    var model = {};

    switch (fieldName) {
        case 'liquorSubType':
            model = GeneralModels.LiquorSubType;
            break;

        case 'liquorMixer':
            model = GeneralModels.LiquorMixer;
            break;

        case 'stipulation':
            model = GeneralModels.Stipulation;
            break;

        case 'drinkContainer':
            model = GeneralModels.DrinkContainer;
            break;
    }


    model.collection.insert(items, function (err, results) {

        if (!err)
            res.send({
                values: results
            });
        else
            console.log(err);

    });

    //GeneralModels.DrinkCategory.collection.insert(categories, function (err, results) {
    //    res.send(results);
    //});
};

exports.actionUpdate = function (req, res) {

    var item = req.body;

    switch (req.params.type) {
        case 'drinkCategory':
            GeneralModels.DrinkCategory.findByIdAndUpdate(
                item._id,
                {
                    $set: {
                        name: item.name
                    }
                },
                function (err, category) {

                    res.send(category);

                });

            break;

        case 'drinkBrand':
            GeneralModels.Brand.findByIdAndUpdate(
                item._id,
                {
                    $set: {
                        name: item.name
                    }
                },
                function (err, brand) {

                    res.send(brand);

                });

            break;


        case 'removeBrandsFromCategory':

            GeneralModels.Brand.update(
                {
                    '_id': {
                        $in: item.brandIds
                    }
                },

                {
                    $pull: {
                        drinkCategories: item.categoryid
                    }

                },

                {
                    multi: true
                },
                function (err) {

                    res.send();

                });

            break;
    }

};

exports.actionRemove = function (req, res) {

    var id = new ObjectId(req.params.id);

    switch (req.params.type) {
        case 'drinkCategory':
            GeneralModels.DrinkCategory.findByIdAndRemove(
                id,
                function (err) {

                    res.send();

                });
            break;

        case 'drinkBrand':
            GeneralModels.Brand.findByIdAndRemove(
                id,
                function (err) {

                    res.send();

                });
            break;
    }

};


exports.addCategoryBrands = function (req, res) {

    var list = req.body.list;
    var type = new ObjectId(req.body.typeid);
    var categoryid = new ObjectId(req.body.categoryid);

    var items = list.match(/[^\r\n]+/g);

    var brands = [];

    for (var i = 0; i < items.length; i++) {
        if (items[i].trim() !== '')
            brands.push({
                name: items[i],
                drinkType: type,
                category: categoryid
            });
    }

    var addBrand = function (brand, callback) {

        GeneralModels.Brand.collection.update(
            {
                name: brand.name
            },
            {
                //if new brand is being created
                $setOnInsert: {
                    name: brand.name,
                    drinkType: brand.drinkType
                },

                //try to add new drink category
                //it will be only added if not existed in array before
                $addToSet: {
                    drinkCategories: brand.category
                }

            },
            {
                upsert: true
            },
            function (err, result) {
                callback(null, result);
            }
        );
    };

    async.map(brands, addBrand, function (e, brands) {

        GeneralModels.Brand.find({drinkType: type, drinkCategories: categoryid}, '-__v ', function (err, brands) {

            res.send(brands);

        });
    });

    //GeneralModels.DrinkCategory.collection.insert(categories, function (err, results) {
    //    res.send(results);
    //});

};


exports.addDrinkBrands = function (req, res) {
    var list = req.body.list;
    var type = req.body.typeid;

    var items = list.match(/[^\r\n]+/g);

    var brands = [];

    for (var i = 0; i < items.length; i++) {
        if (items[i].trim() !== '')
            brands.push({
                name: items[i],
                drinkType: new ObjectId(type)
            });
    }

    GeneralModels.Brand.collection.insert(brands, function (err, results) {
        res.send(results);
    });

};


exports.addDrinkCategories = function (req, res) {
    var list = req.body.list;
    var type = req.body.typeid;

    var items = list.match(/[^\r\n]+/g);

    var categories = [];

    for (var i = 0; i < items.length; i++) {
        if (items[i].trim() !== '')
            categories.push({
                name: items[i],
                drinkType: new ObjectId(type)
            });
    }

    GeneralModels.DrinkCategory.collection.insert(categories, function (err, results) {
        res.send(results);
    });

};

exports.getDeals = function (req, res) {

    var query = req.body.query;

    //modifying string ids into ObjectIds - preparing for mongo query
    for (var key in query) {
        if (query.hasOwnProperty(key))
            query[key] = new ObjectId(query[key]);
    }

    var type = req.body.type;

    switch (type) {
        case 'Beer':

            Deals.BeerDeal.find(query, '-__v').

                populate([
                    {path: 'drinkCategories', select: '_id, name'},
                    {path: 'bar', select: '_id, name'},
                    {path: 'drinkContainers', select: '_id, name'},
                    {path: 'drinkBrands', select: '_id, name'},
                    {path: 'stipulation', select: '_id, name'}
                ])

                .exec(function (err, deals) {

                    res.send(deals);

                });


            break;

        case 'Wine':
            Deals.WineDeal.find(query, '-__v').

                populate([
                    {path: 'drinkCategories', select: '_id, name'},
                    {path: 'bar', select: '_id, name'},
                    {path: 'drinkContainers', select: '_id, name'},
                    {path: 'drinkBrands', select: '_id, name'},
                    {path: 'stipulation', select: '_id, name'}
                ])

                .exec(function (err, deals) {

                    res.send(deals);

                });


            break;

        case 'Liquor':
            Deals.LiquorDeal.find(query, '-__v').

                populate([
                    {path: 'drinkCategories', select: '_id, name'},
                    {path: 'bar', select: '_id, name'},
                    {path: 'liquorSubType', select: '_id, name'},
                    {path: 'cocktailNames', select: '_id, name'},
                    {path: 'drinkBrands', select: '_id, name'},
                    {path: 'stipulation', select: '_id, name'}
                ])

                .exec(function (err, deals) {

                    res.send(deals);

                });

            break;

    }


};