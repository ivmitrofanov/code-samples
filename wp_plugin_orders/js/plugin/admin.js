jQuery(document).ready(function(){

    jQuery('.com').popover();
			
	jQuery('#myModal').modal('hide');
			
	jQuery('.close-form').click(function(){
		jQuery('#myModal').modal('hide');
	});


	var startDate = new Date();
			

	jQuery('.remove_order').live('click', function(){
		var idVal = jQuery(this).attr("id").replace("rm","")
		var el = jQuery(this);
		jQuery.get(ajaxurl ,  
				{action:'ajaxremove', id:idVal} ,
				function(data){
					el.parent().html("<div></div>");
				}
			);
	});


	jQuery('.item-order div').live('click',function(){

		var info = jQuery(this).parent().attr("id").split("_")
		var dateinfo = info[0].split("-"); // date is something like 25-02, we need to convert it into 02-25
		var dateObj = new Date();

		var dateTxt = dateinfo[0]+ "/" + dateinfo[1];
		var year = dateObj.getFullYear();
		if (dateinfo[1] == 1 && dateObj.getMonth() > 1)
			year++;
		
		var dateVal = year + "-"+dateinfo[1] + "-"+dateinfo[0];
		var timeVal = info[1];

		var dateValforDP = info[0] + "-"+year;

		jQuery('#myModal h3').text("Заявка на " + dateTxt);

		//


		jQuery("#names").val("");
   		jQuery("#contact").val("");
   		jQuery("#comments").val("");
		jQuery("#repeat").val(0) ;
		jQuery(".timegroup").removeClass("active");
		jQuery("#t"+timeVal ).addClass("active");

		jQuery("#timefield").val(timeVal)

		jQuery('#dp3').data('date',dateValforDP);
		jQuery("#dateval").val(dateValforDP);
		jQuery('#dp3').datepicker();
		

		if (jQuery(this).text() !=""){
		jQuery.post(ajaxurl ,  
				{action:'ajaxget',date:dateVal, time:timeVal} ,
				function(data) {
					
   					jQuery("#names").val(data.name);
   					jQuery("#contact").val(data.contact);

   					jQuery("#comments").val(data.comments);
					
   					jQuery("#update").val(data.id)
					

   					jQuery('#myModal').modal('show');
				 },
				 "json"
		).error(function() { alert("error"); });
		
		}
		else	jQuery('#myModal').modal('show');
		
	});

	/*
		
*/


	

	jQuery("a.timegroup").bind('click', function(){
	
		var txt = jQuery(this).text();
		jQuery("#timefield").val(jQuery(this).attr("id").replace("t",""));
		jQuery("#pricetable th").parent().css('background','');
		jQuery("#pricetable th:contains('"+txt+"')").parent().css('background','green');
	});

});