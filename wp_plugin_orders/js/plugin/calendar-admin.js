jQuery(document).ready(function () {

    var currentEvent = null;

    jQuery('#calendar').fullCalendar({
        header: {
            right: 'prev,next',
            center: 'title',
            left: ''

        },
        defaultView: 'agendaWeek',
        minTime: "09:00:00",
        //maxTime: "08:00:00",
        defaultDate: currentDate,
        firstDay: startDay,
        allDaySlot: true,
        allDayText: "Ночные сеты",
        lazyFetching: true,
        slotDuration: "1:00:00",
        height: 'auto',
        lang: 'ru',
        weekNumbers: false,
        editable: true,
        eventLimit: false, // allow "more" link when too many events
        loading: function (isLoading, view) {
            if (isLoading) {
                jQuery('.fc-left').html('<img src="/wp-content/plugins/orders/img/ajax-loader.gif" class="loader">')
            }
            else {
                jQuery('.fc-left').html("");
            }
        },

        eventClick: function (event, jsEvent, view) {

            var $originatingTarget = jQuery(jsEvent.target);

            if ($originatingTarget && $originatingTarget.is('.removeOrder')) {

                if (confirm("Уверен, что хочешь удалить этот заказ?")) {
                    jQuery.ajax({
                        url: ajaxurl,
                        type: 'post',
                        dataType: 'json',
                        data: {
                            action: 'removeorder',
                            id: event.id
                        },
                        success: function (data) {
                            //jQuery('#calendar').fullCalendar('refetchEvents');
                            jQuery('#calendar').fullCalendar('removeEvents', event.id);

                        }
                    });
                }

                return;
            }

            currentEvent = event;

            eventModal(event);

			
            jQuery('#myModal').modal('show');
        },

        eventSources: [
            {
                url: ajaxurl,
                type: 'POST',
                data: {
                    action: 'eventsweek',
                    roomid: roomId
                },
                error: function () {
                    alert('there was an error while fetching events!');
                }
            }
        ],

        selectable: true,
        selectHelper: true,
        select: function (start, end) {

            jQuery('#start').val(moment(start).format("HH"));
            var length;

            if (moment(end).diff(moment(start), "days") == 1)
                length = 6;
            else
                length = moment(end).diff(moment(start), "hours")

            jQuery('#length').val(length);
            jQuery('#date').val(moment(start).format("YYYY-MM-DD"));

            eventModal({start: start, end: end, title: '', contact: '', comments: '', id: ''});

            currentEvent = {
                start: start,
                end: end
            };
        },

        eventDrop: function (event, delta, revertFunc) {
            updateEventTimeOnly(event);
        },

        eventResize: function (event, delta, revertFunc) {
            updateEventTimeOnly(event);
        },

        eventRender: function (event, element) {

            if (!event.hasOwnProperty('end') || event.end == null){
                element.prepend('<div class="fc-time"><span>00:00 - 06:00</span></div>');
            }

            element.prepend("<div class='removeOrder' data-id='" + event.id + "'>X</div>");
            if (event.contact != undefined)
                element.append("<div>" + event.contact + "</div>");
            if (event.comments != undefined)
                element.append("<div>" + event.comments + "</div>");
        }

    });

    function eventModal(eventObject) {

        var modalHeader;


        //check for night set
        if (!eventObject.hasOwnProperty('end') || eventObject.end == null || moment(eventObject.end).diff(moment(eventObject.start), "days") == 1)
            modalHeader = moment(eventObject.start).format("D MMMM") + ", ночной сет";
        else
            modalHeader = moment(eventObject.start).format("D MMMM") + " " + moment(eventObject.start).format("HH:mm") + "-" + moment(eventObject.end).format("HH:mm");

        jQuery('#myModal h3').html(modalHeader);
        jQuery('#names').val(eventObject.title);
        jQuery('#contact').val(eventObject.contact);
        jQuery('#comments').val(eventObject.comments.replace(/<br\s*\/?>/mg,""));
        jQuery('#eventid').val(eventObject.id);
		jQuery("#repeat").val(0) ;

        jQuery('#myModal').modal('show');
    }

    function updateEventTimeOnly(event) {

        jQuery.ajax({
            url: ajaxurl,
            type: 'post',
            dataType: 'json',
            data: {
                action: 'updateorder',
                timeOnly: true,
                from: moment(event.start).format("HH"),
                length: moment(event.end).diff(moment(event.start), "hours"),
                date: moment(event.start).format("YYYY-MM-DD"),
                eventid: event.id
            },
            success: function (data) {

            }
        });
    }

    jQuery("#orderform").submit(function (event) {
        event.stopPropagation();

        jQuery('#modalSubmit').attr("disabled", "true");
        jQuery.ajax({
            url: ajaxurl,
            type: 'post',
            dataType: 'json',
            data: jQuery('#orderform').serialize() + "&action=updateorder&roomid=" + roomId,
            success: function (data) {
                jQuery('#modalSubmit').removeAttr("disabled");
                jQuery('#myModal').modal('hide');

                currentEvent.title = jQuery('#names').val();
                currentEvent.contact = jQuery('#contact').val();
                currentEvent.comments = jQuery('#comments').val();

                if (currentEvent.hasOwnProperty('id')) {
                    //update calendar record
                    jQuery('#calendar').fullCalendar('updateEvent', currentEvent);
                }
                else {
                    currentEvent.id = data.id;
                    jQuery('#calendar').fullCalendar('renderEvent', currentEvent, false); // stick? = true

                    jQuery('#calendar').fullCalendar('unselect');
                    jQuery('#calendar').fullCalendar('refetchEvents');

                }

                currentEvent = null;
            }
        });

        return false;
    });

    jQuery('#close-modal').on('click', function (event) {
        event.stopPropagation();
        currentEvent = null;
    })

});