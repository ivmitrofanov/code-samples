function getCalendar(element, roomId) {

    element.fullCalendar({
        header: {
            right: calendarNav,
            center: 'title',
            left: ''

        },
        defaultView: calendarView,
        defaultDate: currentDate,
        firstDay: startDay,
        minTime: "09:00:00",
        allDaySlot: true,
        allDayText: "Ночные сеты",
        axisFormat: 'H:mm' ,

        lazyFetching: true,
        slotDuration: "1:00:00",
        height: 'auto',
        lang: 'ru',
        weekNumbers: false,
        editable: false,
        eventLimit: false, // allow "more" link when too many events

        eventSources: [
            {
                url: ajaxurl,
                color: 'red',
                type: 'POST',
                data: {
                    action: 'week',
                    roomid: roomId
                },
                error: function () {
                    alert('there was an error while fetching events!');
                }
            }
        ],

        selectable: false,

        eventRender: function (event, element) {

            if (!event.hasOwnProperty('end') || event.end == null) {
                element.prepend('<div class="fc-time"><span>00:00 - 06:00</span></div>');
            }
        }

    });
}