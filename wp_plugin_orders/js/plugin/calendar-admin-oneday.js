jQuery(document).ready(function () {

    function getCalendar(element, roomId) {

        element.fullCalendar({
            header: {
                right: '',
                center: 'title',
                left: ''

            },
            defaultView: 'agendaDay',
            defaultDate: currentDate,
            firstDay: startDay,

            lazyFetching: true,
            slotDuration: "1:00:00",
            minTime: "09:00:00",
            allDaySlot: true,
            allDayText: "Ночные сеты",
            height: 'auto',
            lang: 'ru',
            weekNumbers: false,
            editable: false,
            eventLimit: false, // allow "more" link when too many events
            loading: function (isLoading, view) {
                if (isLoading) {
                    jQuery('.fc-left').html('<img src="/wp-content/plugins/orders/img/ajax-loader.gif" class="loader">')
                }
                else {
                    jQuery('.fc-left').html("");
                }
            },

            eventSources: [
                {
                    url: ajaxurl,
                    type: 'POST',
                    data: {
                        action: 'eventsweek',
                        roomid: roomId
                    },
                    error: function () {
                        alert('there was an error while fetching events!');
                    }
                }
            ],

            selectable: false,

            eventRender: function (event, element) {

                if (event.contact != undefined)
                    element.append("<div>" + event.contact + "</div>");
                if (event.comments != undefined)
                    element.append("<div>" + event.comments + "</div>");

                if (!event.hasOwnProperty('end') || event.end == null){
                    element.prepend('<div class="fc-time"><span>00:00 - 06:00</span></div>');
                }
            }


        });
    }

    for (var i = 1; i < 4; i++)
        getCalendar(jQuery("#calendar" + i), i);

});