<?
global $orders_version;
$orders_version = "1.0";

function order_install()
{
    global $wpdb;
    global $orders_version;

    $table_name = $wpdb->prefix . "orders";

    echo "ds";
    if ($wpdb->get_var("show tables like '$table_name'") != $table_name) {

        $sql = "CREATE TABLE IF NOT EXISTS " . $table_name . "(
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `date` date NOT NULL,
        `name` varchar(255) NOT NULL,
        `contact` varchar(255) NOT NULL,
        `order_date` datetime NOT NULL,
        `comments` varchar(255) NOT NULL,
        `timefrom` int(11) NOT NULL,
        `timelength` int(11) NOT NULL,
        `active` tinyint(4) NOT NULL DEFAULT '1',
        `room_id` tinyint(4) NOT NULL DEFAULT '1',
        PRIMARY KEY (`id`)
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);

        add_option("orders_version", $orders_version);

    }

}

