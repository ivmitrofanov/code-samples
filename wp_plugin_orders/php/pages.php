<?
//All orders page
function orders_all()
{
    global $ROOMS;

    echo "<h1>Schedule for today</h1>";
    echo "<div class='row'>";

    get_panel_сalendar('primary', "1", $ROOMS[1]);
    get_panel_сalendar('orange', "2", $ROOMS[2]);
    get_panel_сalendar('success', "3", $ROOMS[3]);

    echo "</div>";

    echo "<script>
        var currentDate = '" . date("Y-m-d") . "';
        var startDay = '" . date("w") . "';
    </script>";

    wp_enqueue_script('calendar-oneday', plugins_url('js/plugin/calendar-admin-oneday.js', dirname(__FILE__)));
}

//page for single room
function orders_rooms()
{
    global $ROOMS;

    $room_id = str_replace("orders", "", get_query_var('page'));

    $room_name = $ROOMS[$room_id];

    echo "<h1>$room_name</h1>";

    //form for adding or changing orders
    echo '
    <div class="modal" id="myModal">
      <div class="modal-header">
        <a class="close close-form" data-dismiss="modal">×</a>
        <h3>Modal header</h3>
      </div>
      <div class="modal-body">
        ' . get_form() . '
      </div>
      <div class="modal-footer">
        <a href="#" class="btn close-form" id="close-modal">Закрыть</a>

      </div>
    </div>
    ';

    echo "<div id='calendar'></div>";

    echo "<script>var currentDate = '" . date("Y-m-d") . "';";
    echo "var startDay = '" . date("w") . "';";
    echo "var roomId = '" . str_replace("orders", "", $_GET['page']) . "';</script>";

    wp_enqueue_script('calendar', plugins_url('js/plugin/calendar-admin.js', dirname(__FILE__)));
}



//panel for each room on Today's schedule
function get_panel_сalendar($css_class, $css_id, $title)
{
    echo '
    <div class="col-md-3 cl-item">
        <div class="panel panel-' . $css_class . '">

            <div class="panel-heading">
                <h3 class="panel-title">' . $title . '</h3>
            </div>

            <div class="panel-body">
                <div id="calendar' . $css_id . '"></div>
            </div>

            <div class="panel-footer">
                <a href="admin.php?page=orders' . $css_id . '">Full schedule &rarr;</a>
            </div>
        </div>
    </div>
    ';

}

//form for adding/updating orders
function get_form($url = "")
{

    return '
<form class="form-inline" action="' . $url . '" id="orderform" name="orderform" method="POST">
    <fieldset>
        <div class="control-group" id="namecontrol">
            <label class="control-label" for="input01">Name:</label>

            <div class="controls">
                <input type="text" class="input" id="names" name="names">

                <p class="help-block"></p>
            </div>
        </div>

        <div class="control-group" id="contactcontrol">
            <label class="control-label" for="input01">Contact data:</label>

            <div class="controls">
                <input type="text" class="input" id="contact" name="contact">

            </div>
        </div>


        <input type="hidden" name="eventid" id="eventid">
        <input type="hidden" name="date" id="date">
        <input type="hidden" name="start" id="start">
        <input type="hidden" name="length" id="length">


        <div class="control-group">
            <label class="control-label" for="textarea">Comment:</label>

            <div class="controls">
                <textarea class="input" rows="3" name="comments" id="comments"></textarea>

            </div>

            <div class="control-group" id="contactcontrol">
                <label class="control-label" for="input01">Every week:</label>

                <div class="controls">
                    <select name="repeat" id="repeat">
                        <option value="0">No</option>
                        <option value="4">4 weeks</option>
                        <option value="8">8 weeks</option>
                        <option value="12">12 weeks</option>
                    </select>

                </div>
            </div>

        </div>


        <div class="form-actions">
            <button type="submit" class="btn btn-primary" id="modalSubmit"> Save</button>

        </div>


    </fieldset>
</form>
			';
}