<?

add_action('wp_ajax_updateorder', 'addupdateorder');
function addupdateorder()
{
    global $wpdb;

    $table_orders = $wpdb->prefix . 'orders';

    //if we are adding new order
    if ($_POST['eventid'] == "") {

        if (empty($_POST['names']))
            return;

        //$wpdb->show_errors();
        $single_order = array(
            $_POST["date"],
            $_POST["names"],
            $_POST["contact"],
            date("Y-m-d h:i"),
            $_POST["comments"],
            $_POST["start"],
            ($_POST["length"] > 0) ? $_POST["length"] : 1,
            1,
            $_POST['roomid']
        );

        $check_query = $wpdb->get_results(
            $wpdb->prepare(
                "SELECT id FROM $table_orders
                WHERE timefrom = %d AND `date` = %s AND active = 1 AND room_id = %d",

                array(
                    $_POST["start"],
                    $_POST["date"],
                    $_POST['roomid']
                )
            )
        );

        if (count($check_query) > 0)
            die("This hours are already taken");


        $total_weeks = $_POST['repeat'] == 0 ? 1 : $_POST['repeat'];

        $time_start = strtotime($_POST["date"]);
        $date_start = date("d-m-Y", $time_start);

        //adding comments for regular clients
        if ($total_weeks > 1)
            $single_order[4] .= "\n" . "Regular client for the next " . $_POST['repeat'] . " weeks. Starts: " . $date_start;

        //adding order for each week
        for ($week = 0; $week < $total_weeks; $week++) {

            if ($week > 0)
                $single_order[0] = date("Y-m-d", strtotime(date("Y-m-d", strtotime($single_order[0])) . " +1 week"));

            $wpdb->query(
                $wpdb->prepare(
                    "INSERT INTO $table_orders
					 (`date`, `name`, contact, order_date, comments, timefrom, timelength, active, room_id)
					  VALUES ( %s, %s, %s,%s,%s,%d, %d, %d, %d)",

                    $single_order
                ));
        }

        //$time = $_POST['start'] . ", for " . $_POST['length'] . " weeks";
        //$headers = 'From: High-Gain Studio <admin@high-gain.ru>' . "\r\n";
        //$message = "Поступил новый заказ\r\nДата: {$single_order[1]}\r\nВремя: $time\r\nКонтактные данные:{$single_order[1]},{$single_order[2]}\r\nКомментарии:{$single_order[4]}";
        //wp_mail("to@mail.com", "New order", $message, $headers);

        die(json_encode(array("id" => $wpdb->insert_id)));

    } else if (!isset($_POST['timeOnly'])) {
        //changing info about client for that order (comments, names)

        if (empty($_POST['names']))
            return;

        $single_order = array(
            $_POST["names"],
            $_POST["contact"],
            $_POST["comments"],
            $_POST['eventid']
        );

        $wpdb->query(
            $wpdb->prepare(
                "UPDATE $table_orders
				SET
				`name` = %s,
				`contact` = %s,
				`comments` = %s

				WHERE id = %d
			",
                $single_order
            )
        );
    } else { //changing date and time (moving order in the UI)

        $single_order = array(
            $_POST["date"],
            $_POST["from"],
            ($_POST["length"] > 0) ? $_POST["length"] : 1,
            $_POST['eventid']
        );


        $wpdb->query(
            $wpdb->prepare(
                "UPDATE $table_orders
				SET
				`date` = %s,
				`timefrom` = %s,
				`timelength` = %s

				WHERE id = %d
			",
                $single_order
            )
        );

    }

    die();
}

//for non-logged users
add_action('wp_ajax_nopriv_week', 'week');
add_action('wp_ajax_week', 'week');
function week()
{
    getEvents("user");
}


//for logged-in users
add_action('wp_ajax_eventsweek', 'eventsweek');
function eventsweek()
{
    getEvents("admin");
}


function getEvents($mode = "admin")
{
    global $wpdb;

    $from_date = $_POST['start'];
    $end_date = $_POST['end'];
    $room_id = intval($_POST['roomid']);

    $table_orders = $wpdb->prefix . 'orders';

    $order_results = $wpdb->get_results(
        $wpdb->prepare(
            "SELECT * FROM $table_orders
            WHERE (`date` BETWEEN  %s - INTERVAL 1 DAY AND %s)
			AND `active` = 1 AND room_id = %d
            ORDER BY id",

            $from_date,
            $end_date,
            $room_id
        )
    );

    $events = array();
    foreach ($order_results as $order_row) {

        //preparing data for fullcalendar format
        $end = new DateTime($order_row->date . ' ' . $order_row->timefrom . ':00:00');
        $end->modify('+' . $order_row->timelength . ' hours');

        if ($order_row->timefrom == 0)
            $end_string = "";
        else
            $end_string = $end->format('Y-m-d') . 'T' . $end->format("H:i:s");

        $formatted_timefrom = sprintf("%02d", $order_row->timefrom);

        if ($mode == "admin")
            $order = array(
                'id' => $order_row->id,
                'title' => $order_row->name,
                'contact' => $order_row->contact,
                'start' => $order_row->date . 'T' . $formatted_timefrom . ':00:00',
                //'end' => $end_string,
                'comments' => nl2br($order_row->comments),

            );
        else
            $order = array(
                'id' => $order_row->id,
                'title' => '',
                'start' => $order_row->date . 'T' . $formatted_timefrom . ':00:00',
                //'end' => $end_string,

            );

        if ($order_row->timefrom != 0)
            $order['end'] = $end_string;
        else
            $order['allDay'] = true;

        $events[] = $order;

    }

    echo json_encode($events);

    die();
}


add_action('wp_ajax_removeorder', 'removeorder');
function removeorder()
{
    global $wpdb;
    $id = $_POST["id"];
    $table_orders = $wpdb->prefix . 'orders';

    if (is_numeric($id)) {
        $wpdb->get_results($wpdb->prepare(
                "
				UPDATE $table_orders
				SET active = 0
				WHERE id = %d

			",

                $id
            )
        );
    }
    die();
}
