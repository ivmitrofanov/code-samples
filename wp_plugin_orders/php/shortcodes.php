<?

add_shortcode('order', 'order_shortcode');

// Shortcode for displaying calendar on the site
function order_shortcode($params = "", $content = "")
{
    global $ROOMS;

    wp_enqueue_script('moment', plugins_url('js/common/moment.min.js', dirname(__FILE__)));
    wp_enqueue_script('fullcalendar', plugins_url('js/common/fullcalendar.min.js', dirname(__FILE__)));
    wp_enqueue_script('fullcalendar-ru', plugins_url('js/common/lang/ru.js', dirname(__FILE__)));

    wp_enqueue_style('orders', plugins_url('css/orders.css', dirname(__FILE__)));
    wp_enqueue_style('calendar', plugins_url('css/fullcalendar.min.css', dirname(__FILE__)));

    $res = '';

    if (is_array($params)) {
        if ($params['room'] == 'all') {


            $res .= '<!-- Nav tabs -->
   
						<div class="align-center">
					   
							<ul class="nav nav-pills tabs" role="tablist">
							  <li role="presentation" class="active"><a href="#room1" class="tab-link" role="tab" data-toggle="tab" data-calendar="calendar1">' . $ROOMS[1] . '</a></li>
							  <li role="presentation"><a href="#room3" role="tab"  class="tab-link" data-toggle="tab" data-calendar="calendar3">' . $ROOMS[3] . '</a></li>
							  <li role="presentation"><a href="#room2" role="tab" class="tab-link"  data-toggle="tab" data-calendar="calendar2">' . $ROOMS[2] . '</a></li>
							  
							</ul>
						</div>
						
						<!-- Tab panes -->
						<div class="tab-content">
						  <div role="tabpanel" class="tab-pane active" id="room1"><div id="calendar1" class="clnd"></div></div>
						  <div role="tabpanel" class="tab-pane" id="room2"><div id="calendar2" class="clnd"></div></div>
						  <div role="tabpanel" class="tab-pane" id="room3"><div id="calendar3" class="clnd"></div></div>
						  <div role="tabpanel" class="tab-pane" id="room4"><div id="calendar4" class="clnd"></div></div>
						</div>';

            //calendar needs to be rerendered each time tab when is getting activated
            $res .= "<script>jQuery(document).ready(function () {
						jQuery('.tab-link').on('shown.bs.tab', function (event) {

							jQuery('#' + jQuery(this).data('calendar')).fullCalendar('render');
						});

						for (var i = 1; i < 4; i++)
							getCalendar(jQuery('#calendar' + i), i);
						});	
					</script>";
        } else {
            $id = intval($params['room']);

            $res .= '<div id="calendar" class="clnd"></div>';
            $res .= "<script>jQuery(document).ready(function () {
                        getCalendar(jQuery('#calendar'), " . $id . ");
					});
					</script>";
        }
    }


    $res .= "<script>
    var currentDate = '" . date("Y-m-d") . "';
    var calendarView = $(window).width() < 1024 ? 'agendaDay' : 'agendaWeek';
    var calendarNav = $(window).width() < 1024  ? 'prev,next' : '';
    var startDay = '" . date("w") . "';
    </script>";

    wp_enqueue_script('calendar-user', plugins_url('js/plugin/calendar.js', dirname(__FILE__)));

    return $res;
}