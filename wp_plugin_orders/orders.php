<?php
/*
Plugin Name: Simple orders plugin
Plugin URI: http://high-gain.ru
Description: Simple orders schedule plugins
Version: 1.0
Author: Igm
Author URI: http://igmit.com
License: GPL
*/

$ROOMS = array(
    1 => "Big room",
    2 => "Small room",
    3 => "Middle room",
);

include("php/install.php");
include("php/pages.php");
include("php/shortcodes.php");
include("php/ajaxcalls.php");


register_activation_hook(__FILE__, 'order_main_install');
function order_main_install(){
    order_install();
}


add_action('admin_enqueue_scripts', 'orders_admin_scripts');
function orders_admin_scripts($hook_suffix) {

    //show js and css only on plugin pages
    if ($hook_suffix !== "toplevel_page_orders" && strpos($hook_suffix, 'orders_page_orders') !== 0)
       return;

    wp_enqueue_script('moment', plugins_url('js/common/moment.min.js', __FILE__));
    wp_enqueue_script('fullcalendar', plugins_url('js/common/fullcalendar.min.js', __FILE__));
    wp_enqueue_script('fullcalendar-ru', plugins_url('js/common/lang/ru.js', __FILE__));
    wp_enqueue_script('bootstrap-tooltip', plugins_url('js/common/bootstrap-tooltip.js', __FILE__));
    wp_enqueue_script('bootstrap-button', plugins_url('js/common/bootstrap-button.js', __FILE__));
    wp_enqueue_script('bootstrap-popover', plugins_url('js/common/bootstrap-popover.js', __FILE__));
    wp_enqueue_script('bootstrap-modal', plugins_url('js/common/bootstrap-modal.js', __FILE__));
    wp_enqueue_script('orders-admin', plugins_url('js/plugin/admin.js?ver=' . date("dmyhi"), __FILE__));

    wp_enqueue_style('bootstrap', plugins_url('css/bootstrap.css', __FILE__));
    //wp_enqueue_style('datepicker', plugins_url('css/datepicker.css', __FILE__));
    wp_enqueue_style('orders', plugins_url('css/orders.css', __FILE__));
    wp_enqueue_style('calendar', plugins_url('css/fullcalendar.min.css', __FILE__));

}

add_action('admin_menu', 'orders_admin_menu');
function orders_admin_menu()
{
    global $ROOMS;

    add_object_page('Orders page', 'Orders', 'edit_posts', 'orders', 'orders_all');

    foreach ($ROOMS as $id => $name)
        add_submenu_page('orders', 'Rooms', $name, 'edit_posts', 'orders' . $id, 'orders_rooms');
}


add_action('wp_footer', 'orders_footer');
function orders_footer()
{
    echo "<script>var ajaxurl = '" . admin_url('admin-ajax.php') . "';</script>";
}
