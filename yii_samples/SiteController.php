<?php

class SiteController extends Controller
{

    public $cart_obj;
    public $cart_info;

    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        $goods = Goods::model()->findAllByAttributes(array(
            'is_best_of_week' => 1
        ));

        $goods_promoted = Goods::model()->findAllByAttributes(array(
            'is_promoted' => 1
        ));
        $this->readCartCookie();

        $this->render('index', array('goods' => $goods, 'goods_promoted' => $goods_promoted));
    }

    public function actionList()
    {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'

        $this->readCartCookie();
        //prepare structured by category goods array
        $goods_structured = array();

        $goods = Goods::model()->findAllByAttributes(array(
            'is_promoted' => 0
        ));


        if ($goods) {
            foreach ($goods as $good) {
                $goods_structured[$good->category->name][] = $good;
            }

        }

        $this->render('list', array('goods' => $goods_structured));
    }


    public function actionSendemail()
    {
        if (Yii::app()->request->isPostRequest) {

            $name = Yii::app()->getRequest()->getPost('email_name');
            $date = Yii::app()->getRequest()->getPost('email_date');
            $time = Yii::app()->getRequest()->getPost('email_time');

            $to = Yii::app()->getRequest()->getPost('b_email');
            $id = Yii::app()->getRequest()->getPost('order_id');

            $orderinfo = OrdersInfo::model()->findByPk($id);

            $orderinfo->email = $to;
            $orderinfo->save();

            $order = Orders::model()->findAllByAttributes(array('orders_info_id' => $id));

            $order_text = 'Orden ID: #' . $orderinfo->orders_info_id . "<br>";
            $total = 0;

            foreach ($order as $o) {
                $order_text .= "<i>" . $o->items_count . " x " . $o->good->product_name . " - <b>$ " . $o->price_info->price . "</b></i><br>";
                $total += $o->price_info->price * $o->items_count;
            }

            $order_text .= "<u><b>Total: $ " . $total . "</b></u>";

            $subject = "Confirmación de la orden - vinos-de-leon.com";
            $from = "contact@vinos-de-leon.com";
            $text = "<p>Hola $name!</p>
					 <p>Has creado una orden en nuestra página web vinos-de-leon.com!</p>
					 <p>Haremos la entrega el $date desde $time.</p>
					 <p>Su orden:</p>
					 <p>$order_text</p>
					 <p>&nbsp;</p>
					 <p>Atentamente,<br>
					Vinos-De-Leon.com (contact@vinos-de-leon.com)
					 </p>
			";

            $headers = 'From: Ana De Leon <' . $from . ">\r\n" .
                'Reply-To: ' . $from . "\r\n" .
                "Content-type: text/html\r\n" .
                'X-Mailer: PHP/' . phpversion();

            mail($to, $subject, $text, $headers);

            echo "1";
        }
    }


    private function readCartCookie()
    {

        $cookie = Yii::app()->request->cookies;
        $this->cart_info = array();
        $this->cart_info['total'] = 0;

        if (isset($cookie['cookieCart'])) {

            $cart = json_decode(stripcslashes((string)$cookie['cookieCart']), true);

            foreach ($cart as $product_id => $product) {
                if ($product)
                    foreach ($product as $volume_size => $info) {
                        $volume = Volume::model()->findByAttributes(array('volume_size' => $volume_size));

                        $good_info = GoodAttributes::model()->findByAttributes(
                            array('volume_id' => $volume->volume_id,
                                'good_id' => $product_id
                            )
                        );

                        if (count($good_info) == 1) {
                            $cart[$product_id][$volume_size][1] = $good_info->price;
                            $this->cart_info[$product_id]['name'] = $good_info->good->product_name;
                            $this->cart_info[$product_id]['picture'] = $good_info->good->picture;
                            $this->cart_info['total'] += $good_info->price * $info[0];
                        } else {
                            unset($cart[$product_id][$volume_size]);

                        }


                    }

                if (count($cart[$product_id]) == 0)
                    unset($cart[$product_id]);
            }


            $this->cart_obj = $cart;

            $cookie_new = new CHttpCookie('cookieCart', json_encode($this->cart_obj));
            $cookie_new->expire = time() + 60 * 60 * 24 * 7;
            $cookie_new->path = '/';

            Yii::app()->request->cookies['cookieCart'] = $cookie_new;


        }
    }

    public function actionSendorder()
    {

        if (Yii::app()->request->isPostRequest) {

            $orderInfo = new OrdersInfo();
            $orderInfo->name = Yii::app()->getRequest()->getPost('b_name');
            $orderInfo->address = Yii::app()->getRequest()->getPost('b_address');
            $orderInfo->phone = Yii::app()->getRequest()->getPost('b_phone');
            $orderInfo->delivery_date = Yii::app()->getRequest()->getPost('b_date');
            $orderInfo->delivery_time = Yii::app()->getRequest()->getPost('b_time');
            $orderInfo->comment = Yii::app()->getRequest()->getPost('b_comment');
            $orderInfo->order_date = date('Y-m-d H:i:s');

            $orderInfo->save();

            $cart = json_decode(Yii::app()->getRequest()->getPost('cart'), true);

            foreach ($cart as $product_id => $product) {
                if ($product)
                    foreach ($product as $volume_size => $info) {
                        $order = new Orders();
                        $order->good_id = $product_id;
                        $order->orders_info_id = $orderInfo->orders_info_id;
                        $order->items_count = $info[0];
                        $volume = Volume::model()->findByAttributes(array('volume_size' => $volume_size));
                        $order->volume_id = $volume->volume_id;
                        $order->save();
                    }
            }


            $result = array("order_id" => $orderInfo->orders_info_id);

            echo json_encode($result);;

        }

    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        $this->readCartCookie();
        if ($error = Yii::app()->errorHandler->error) {


            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', array('items_left' => getItemsLeft(), 'error' => $error));
        }
    }


}