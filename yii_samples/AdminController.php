<?php

class AdminController extends Controller
{
    /**
     * Declares class-based actions.
     */
    public $layout = 'layout_admin';

    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function actionProduct($id = '')
    {
        if (Yii::app()->user->id != 1)
            return;


        if ($id != '')
            $good = Goods::model()->findByPk($id);
        else
            $good = new Goods;

        $criteria = new CDbCriteria;
        $criteria->order = 'volume_size DESC';

        $volumes_structured = array();
        $volumes = Volume::model()->findAll($criteria);
        $volumes_id_info = array();

        if ($volumes)
            foreach ($volumes as $volume) {

                $volumes_structured[$volume->volume_size] = '';
                $volumes_id_info[$volume->volume_size] = $volume->volume_id;
            }

        $errors = array();

        if (Yii::app()->request->isPostRequest) {
            $request = Yii::app()->getRequest();


            $good->product_name = $request->getPost('product_name');
            $good->product_description = $request->getPost('product_description');
            $good->is_active = $request->getPost('is_active');
            $good->category_id = $request->getPost('category_id');

            $good->is_best_of_week = 0;
            $good->is_promoted = 0;

            if ($request->getPost('promotion')) {
                if (in_array(0, $request->getPost('promotion')))
                    $good->is_best_of_week = 1;

                if (in_array(1, $request->getPost('promotion')))
                    $good->is_promoted = 1;

            }

            $pictureFile = CUploadedFile::getInstanceByName('picture');
            $promoteFile = CUploadedFile::getInstanceByName('picture_promoted');

            if ($good->save()) {
                $id = $good->good_id;
                $save_flag = false;

                foreach ($request->getPost('volumes') as $size => $price) {

                    $product_info = GoodAttributes::model()->findByAttributes(
                        array(
                            'volume_id' => $volumes_id_info[$size],
                            'good_id' => $id
                        )
                    );

                    if (count($product_info) == 0 && !empty($price))
                        $product_info = new GoodAttributes;

                    if (!empty($price)) {
                        $product_info->volume_id = $volumes_id_info[$size];
                        $product_info->good_id = $id;
                        $product_info->price = $price;
                        $product_info->save();
                    } else if (count($product_info) == 1 && empty($price)) {

                        $product_info->delete();
                    }


                }

                //check for images to be uploaded

                if (!empty($pictureFile)) {
                    $save_flag = true;

                    $path = Yii::app()->basePath . '/../product_images/';
                    $picture_name = 'original/' . $id . '_' . date("dmYHi") . $pictureFile->name;
                    $pictureFile->saveAs($path . $picture_name);

                    $good->picture = $id . ".jpg";

                    $image = Yii::app()->image->load($path . $picture_name);
                    $image->resize(150, 150, Image::AUTO)->quality(75);

                    if (file_exists($path . 'thumb/' . $good->picture))
                        unlink($path . 'thumb/' . $good->picture);

                    $image->save($path . 'thumb/' . $good->picture);

                    $image_jpg = Yii::app()->image->load($path . $picture_name);
                    $image_jpg->quality(75);
                    $image_jpg->save($path . $good->picture);
                }

                if (!empty($promoteFile)) {
                    $save_flag = true;

                    $path = Yii::app()->basePath . '/../slides/';
                    $picture_name = $id . '_' . date("dmYHi") . $promoteFile->name;

                    if (file_exists($path . $picture_name))
                        unlink($path . $picture_name);

                    $promoteFile->saveAs($path . $picture_name);
                    $good->picture_promoted = $picture_name;

                }

                //save file names in database
                if ($save_flag) {
                    if ($good->save()) {
                        $this->redirect(Yii::app()->baseUrl . "/admin/product/" . $id . '/?saved=1');
                    } else
                        $errors = $good->getErrors();

                } else
                    $this->redirect(Yii::app()->baseUrl . "/admin/product/" . $id . '/?saved=1');

            } else {
                $errors = $good->getErrors();
            }


            //
        }

        $categories = Categories::model()->findAll();

        foreach ($good->volumes as $vl) {
            $volumes_structured[$vl->volume->volume_size] = $vl->price;

        }
        ksort($volumes_structured);

        $this->render('goods/goodform',
            array(
                'model' => $good,
                'volumes' => $volumes_structured,
                'categories' => $categories,
                'errors' => $errors
            ));
    }

    public function actionBestofweek()
    {
        if (Yii::app()->user->id != 1)
            return;

        //prepare structured by category goods array
        $goods_structured = array();

        $goods = Goods::model()->findAllByAttributes(array(
            'is_best_of_week' => 1
        ));

        $this->render('goods/bestofweek', array('goods' => $goods));
    }

    public function actionPromoted()
    {
        if (Yii::app()->user->id != 1)
            return;

        //prepare structured by category goods array
        $goods_structured = array();

        $goods = Goods::model()->findAllByAttributes(array(
            'is_promoted' => 1
        ));

        $this->render('goods/promoted', array('goods' => $goods));
    }

    //ajax for best of week
    public function actionBow($id)
    {
        if (Yii::app()->user->id != 1)
            return;

        $goods = Goods::model()->findByPk($id);
        $goods->is_best_of_week = (int)!$goods->is_best_of_week;
        $goods->save();

        echo $goods->is_best_of_week;
    }

    public function actionShowallgoods()
    {
        if (Yii::app()->user->id != 1)
            return;

        //prepare structured by category goods array
        $goods_structured = array();

        $goods = Goods::model()->findAll();

        if ($goods) {
            foreach ($goods as $good) {
                $goods_structured[$good->category->name][] = $good;
            }

        }

        $this->render('goods/showall', array('goods' => $goods_structured));
    }

    public function actionDelete($id)
    {
        if (Yii::app()->user->id != 1) {
            echo "error";
            die();
        }
        $order = OrdersInfo::model()->findByPk($id);
        $order->is_removed = 1;
        $order->save();
        echo "ok";
    }

    public function actionComplete($id)
    {
        if (Yii::app()->user->id != 1) {
            echo "error";
            die();
        }

        $order = OrdersInfo::model()->findByPk($id);
        $order->is_completed = 1;
        $order->save();
        echo "ok";

    }

    public function actionOrderhistory()
    {
        $criteria = new CDbCriteria;
        $criteria->condition = " is_removed=0";
        $criteria->order = 'order_date DESC';
        $criteria->limit = 100;

        $orders = OrdersInfo::model()->findAll($criteria);

        foreach ($orders as $v) {
            $orders_prepared[date("d-m-Y", strtotime($v->order_date))][] = $v;
        }

        $this->render('orderhistory', array('orders' => $orders_prepared));
    }

    public function actionOrderlist($date = "")
    {
        if (Yii::app()->user->id != 1)
            return false;

        if (date("N") < 6)
            $current_date = date("Y-m-d", strtotime("saturday"));

        else
            $current_date = date("Y-m-d");


        $criteria = new CDbCriteria;
        $criteria->select = array('delivery_date');
        $criteria->condition = "delivery_date >= '" . $current_date . "' and is_removed=0";
        $criteria->group = 'delivery_date';
        $criteria->order = 'delivery_date';
        $criteria->limit = 4;

        $next_dates = OrdersInfo::model()->findAll($criteria);

        $next_dates_prepared = array();

        $i = 0;
        foreach ($next_dates as $v) {
            $next_dates_prepared[$i]['date'] = $v->delivery_date;
            $next_dates_prepared[$i]['date_str'] = date("l, d M", strtotime($v->delivery_date));
            $next_dates_prepared[$i]['active'] = $v->delivery_date == $date ? ' active' : '';


            $i++;
        }

        $criteria = new CDbCriteria;
        if ($date != "") {
            $criteria->condition = "delivery_date = '" . $date . "' and is_removed=0";
        } else {
            $criteria->condition = "is_removed=0";
        }
        $criteria->order = 'delivery_time, name ASC';

        $orders = OrdersInfo::model()->findAll($criteria);

        $orders_prepared = array();
        foreach ($orders as $o) {
            $orders_prepared[$o->delivery_time][] = $o;
        }

        $this->render('orderlist', array('orders' => $orders_prepared, 'dates' => $next_dates_prepared, 'date' => $date));
    }

    public function accessRules()
    {
        return array(
            array('deny',
                'actions' => array('create', 'edit'),
                'users' => array('?'),
            ),
            array('allow',
                'actions' => array('delete'),
                'roles' => array('admin'),
            ),
            array('deny',
                'actions' => array('delete'),
                'users' => array('*'),
            ),
        );
    }


    public function actionIndex()
    {
        //$this->render('index');
    }


    /**
     * Displays the login page
     */
    public function actionLoginscb()
    {
        $model = new LoginForm;

        $form = new LoginForm;
        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $form->attributes = $_POST['LoginForm'];
            // validate user input and redirect to previous page if valid
            if ($form->validate() && $form->login()) $this->redirect(Yii::app()->baseUrl . "/admin/orderlist");
        }
        // display the login form
        $this->render('login', array('model' => $form));

    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->baseUrl . "/admin/login");
    }
}